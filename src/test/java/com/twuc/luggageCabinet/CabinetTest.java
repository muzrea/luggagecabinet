package com.twuc.luggageCabinet;

import com.twuc.luggageCabinet.exceptions.InvalidTicket;
import com.twuc.luggageCabinet.exceptions.OverCapacity;
import com.twuc.luggageCabinet.exceptions.SizeLargerThanLocker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class CabinetTest {

    private Cabinet cabinet;

    @BeforeEach
    void setUp() {
        cabinet = new Cabinet(3);
    }

    @Test
    void should_return_a_ticket_when_deposit_luggage() {
        Ticket ticket = cabinet.deposit(new Luggage(),LockerSize.LARGE);
        assertNotNull(ticket);
    }

    @Test
    void should_return_luggage_when_use_ticket() {
        Luggage myLuggage = new Luggage();
        Ticket ticket = cabinet.deposit(myLuggage,LockerSize.LARGE);
        Luggage claimedLuggage = cabinet.claim(ticket);
        assertEquals(myLuggage, claimedLuggage);
    }

    @Test
    void should_return_first_luggage_when_deposit_two_luggage() {
        Luggage firstLuggage = new Luggage();
        Ticket ticket = cabinet.deposit(firstLuggage,LockerSize.LARGE);
        cabinet.deposit(new Luggage(LuggageSize.LARGE),LockerSize.LARGE);

        Luggage claimedLuggage = cabinet.claim(ticket);

        assertEquals(firstLuggage, claimedLuggage);
    }

    @Test
    void should_return_null_when_deposit_null_luggage() {
        Ticket ticket = cabinet.deposit(null,LockerSize.LARGE);

        Luggage claimLuggage = cabinet.claim(ticket);
        assertNull(claimLuggage);
    }

    @Test
    void should_throw_illegalException_when_claim_use_illegal_ticket() {
        assertThrows(InvalidTicket.class, () -> cabinet.claim(new Ticket()));
    }

    @Test
    void should_throw_exception_when_ticket_is_used() {
        Ticket ticket = cabinet.deposit(new Luggage(),LockerSize.LARGE);
        cabinet.claim(ticket);

        assertThrows(InvalidTicket.class, () -> cabinet.claim(ticket));
    }

    @Test
    void should_throw_over_capacity_error_when_luggage_more_than_capacity() {
        IntStream.range(0, 3).forEach(i -> cabinet.deposit(new Luggage(),LockerSize.LARGE));

        assertThrows(OverCapacity.class, () -> cabinet.deposit(new Luggage(LuggageSize.LARGE),LockerSize.LARGE));
    }

    @Test
    void should_throw_exception_when_luggage_is_larger_than_locker() {
        Luggage luggage = new Luggage(LuggageSize.EXTRA_LARGE);
        assertThrows(SizeLargerThanLocker.class, () -> cabinet.deposit(luggage,LockerSize.LARGE));
    }

    @Test
    void should_deposit_successfully_when_luggage_is_smaller_than_locker() {
        Luggage luggage = new Luggage(LuggageSize.MEDIUM);
        Ticket ticket = cabinet.deposit(luggage,LockerSize.LARGE);
        Luggage claimedLuggage = cabinet.claim(ticket);
        assertEquals(luggage, claimedLuggage);
    }
}
