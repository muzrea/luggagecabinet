package com.twuc.luggageCabinet;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class LuggageTest {
    @ParameterizedTest
    @EnumSource(
            value = LuggageSize.class,
            names = {"EXTRA_LARGE","LARGE","MEDIUM","SMALL","MINI"}
    )
    void should_return_luggage_size(LuggageSize size) {
        Luggage luggage = new Luggage(size);
        assertEquals(size, luggage.getLuggageSize());
    }
}