package com.twuc.luggageCabinet;

public enum LuggageSize {
    EXTRA_LARGE(5),LARGE(4),MEDIUM(3),SMALL(2),MINI(1);

    private int weight;

    LuggageSize(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isLargerThan(LockerSize lockerSize) {
        return this.weight > lockerSize.getWeight();
    }
}
