package com.twuc.luggageCabinet;

public class Luggage {
    private LuggageSize luggageSize;

    public Luggage(){
        large();
    }

    public Luggage(LuggageSize luggageSize) {

        this.luggageSize = luggageSize;
    }

    public LuggageSize getLuggageSize() {
        return this.luggageSize;
    }

    public LuggageSize extraLarge() {
        this.luggageSize = LuggageSize.EXTRA_LARGE;
        return LuggageSize.EXTRA_LARGE;
    }

    public LuggageSize large() {
        this.luggageSize = LuggageSize.LARGE;
        return LuggageSize.LARGE;
    }
}
