package com.twuc.luggageCabinet;

import com.twuc.luggageCabinet.exceptions.InvalidTicket;
import com.twuc.luggageCabinet.exceptions.OverCapacity;
import com.twuc.luggageCabinet.exceptions.SizeLargerThanLocker;

import java.util.HashMap;
import java.util.Map;

public class Cabinet {
    private Map<Ticket,Luggage> lockers = new HashMap<Ticket,Luggage>();
    private int capacity;

    public Cabinet(int capacity) {
        this.capacity = capacity;
    }

    public Ticket deposit(Luggage luggage,LockerSize lockerSize) {
        if(luggage != null){
            if(luggage.getLuggageSize().isLargerThan(lockerSize)){
                throw new SizeLargerThanLocker();
            }
        }
        if(this.lockers.size() >= this.capacity){
            throw new OverCapacity();
        }
        Ticket ticket = new Ticket();
        this.lockers.put(ticket,luggage);
        return ticket;
    }

    public Luggage claim(Ticket ticket) {
        if(!this.lockers.containsKey(ticket)){
            throw new InvalidTicket();
        }
        return this.lockers.remove(ticket);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }
}
